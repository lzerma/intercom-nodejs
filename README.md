Intercom - The Test
========

[![CircleCI](https://circleci.com/bb/lzerma/intercom-nodejs/tree/master.svg?style=svg)](https://circleci.com/bb/lzerma/intercom-nodejs/tree/master)

#### What's your proudest achievement? It can be a personal project or something you've worked on professionally. Just a short paragraph is fine, but I'd love to know why you're proud of it, what impact it had (If any) and any insights you took from it.
My proudest achievement was an integration I was leading when working back in Brazil on a Malapronta, the project was very challenging and with tight deadlines, the project went smoothly, although we 
had to use some tools that, at that time, weren't that mature just yet, thus, making the project a really good use case for tools such as message brokers, NoSql databases and event driven languages.
One of the reasons I'm proud of this achievement was due the fact that I was leading the project and we delivered it on time and we brought 20% more profit to the company increasing the number hotels available on the channel.
   
#### Write a function that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] → [1,2,3,4]. If the language you're using has a function to flatten arrays, you should pretend it doesn't exist.
[Flatten array function](https://bitbucket.org/lzerma/intercom-nodejs/src/0a57fe7bac79ecc38d7cb16ca34fd08d42793afb/util/arrayHandler.js?at=master&fileviewer=file-view-default)

#### We have some customer records in a text file (customers.json) -- one customer per line, JSON-encoded. We want to invite any customer within 100km of our Dublin office for some food and drinks on us. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).
 - You can use the first formula from this Wikipedia article to calculate distance. Don't forget, you'll need to convert degrees to radians.
 - The GPS coordinates for our Dublin office are 53.3393,-6.2576841.
 - You can find the Customer list here.