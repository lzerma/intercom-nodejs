const { readFile, arrayifyFileContents } = require('../util/fileHandler');
const { calculateDistance } = require('../util/distanceCalculationHandler');

const loadCustomers = (path) => {
  path = (!path) ? 'files/customers_list.txt' : path;
  const fileContents = readFile(path);
  return arrayifyFileContents(fileContents);
};

const calcCustomersDistToDestiny = (customers, destiny) => {
  if (!Array.isArray(customers) && typeof point !== 'object') {
    throw new TypeError('`customers` or `point` must be objects.');
  }

  customers.map((customer) => {
    customer.distance = calculateDistance(
      destiny.latitude,
      destiny.longitude,
      Number(customer.latitude),
      Number(customer.longitude)
    );
    return customer;
  });
  return customers;
};

const get = (req, res) => {
  let customers = loadCustomers();
  const officeLocation = { latitude: 53.3393, longitude: -6.2576841 };
  customers = calcCustomersDistToDestiny(customers, officeLocation);
  const radiusLimit = 100; // 100km radius

  customers = customers.filter((customer) => {
    if (customer.distance > radiusLimit) {
      return false;
    }
    return true;
  }).sort((customerA, customerB) => customerA.user_id - customerB.user_id).map(customer => ({
    name: customer.name,
    user_id: customer.user_id
  }));
  res.json(customers);
};

module.exports = { get, loadCustomers, calcCustomersDistToDestiny };
