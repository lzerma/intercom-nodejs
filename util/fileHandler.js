const fs = require('fs');

function isJsonString(str) {
  try {
    return JSON.parse(str);
  } catch (e) {
    return false;
  }
}

const readFile = (path) => {
  if (typeof path === 'string') {

    // throws an error if path does not exists. ENOENT
    return fs.readFileSync(path, 'utf8');
  }

  throw new TypeError('argument path must be a string.');
};

const arrayifyFileContents = (contents) => {
  if (typeof contents === 'string') {
    return contents.split('\n').map((row) => {
      const json = isJsonString(row);
      return json || row;
    });
  }

  throw new TypeError('argument path must be a string.');

};

module.exports = { isJsonString, readFile, arrayifyFileContents };
