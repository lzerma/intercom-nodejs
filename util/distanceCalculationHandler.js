const calculateDistance = (latidudeFrom, longitudeFrom, latitudeTo, longitudeTo) => {
  if (
    !isNaN(latidudeFrom) && !isNaN(latitudeTo) &&
    !isNaN(longitudeFrom) && !isNaN(longitudeTo)) {

    const R = 6371; // Earth radius

    latidudeFrom *= Math.PI / 180;
    longitudeFrom *= Math.PI / 180;

    latitudeTo *= Math.PI / 180;
    longitudeTo *= Math.PI / 180;

    const diffLongitude = Math.abs(longitudeFrom - longitudeTo);

    return (R * Math.acos(
      (Math.sin(latidudeFrom) * Math.sin(latitudeTo)) +
      ((Math.cos(latidudeFrom) * Math.cos(latitudeTo)) *
        Math.cos(diffLongitude))));
  }
  throw new TypeError('Arguments given must be Numbers.');
};

module.exports = { calculateDistance };
