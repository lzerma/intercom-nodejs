const flattenArray = (integerList, flattenedIntegerList) => {
  flattenedIntegerList = (Array.isArray(flattenedIntegerList))
    ? flattenedIntegerList
    : [];

  if (Array.isArray(integerList)) {
    integerList.forEach((elem) => {
      if (Array.isArray(elem)) {
        flattenArray(elem, flattenedIntegerList);
      } else {
        flattenedIntegerList.push(elem);
      }
    });
    return flattenedIntegerList;
  }

  throw new TypeError('integerList must be an Array.');
};
module.exports = { flattenArray };
