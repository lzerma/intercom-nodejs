const expect = require('chai').expect;
const { isJsonString, readFile, arrayifyFileContents } = require('../../../util/fileHandler');

const sampleFileContents = '2017 this should have be the same always. @.12340&,./';
const sampleFile2Contents = '1\n2\n3\n4\n5';
const sampleJsonString = '{"string": "1", "number": 12, "object": {"string":"string"}, "bool": true}';
const sampleJson = {
  string: '1',
  number: 12,
  object: { string: 'string' },
  bool: true
};

describe('File handler suite.', () => {
  describe('isJsonString scenarios.', () => {
    it('should be not null', () => {
      expect(isJsonString).to.not.be.null;
    });
    it('should return false if is not a valid json string', () => {
      expect(isJsonString(sampleFileContents)).to.be.false;
    });
    it('should return the parsed json if is a valid json string', () => {
      expect(isJsonString(sampleJsonString)).to.be.deep.equal(sampleJson);
    });
  });
  describe('readFile scenarios.', () => {
    it('should be not null', () => {
      expect(readFile).to.not.be.null;
    });
    it('should throw error if path isn\'t a string', () => {
      expect(() => readFile({})).to.throw(TypeError);
    });
    it('should throw error if path isn\'t a does not exist', () => {
      expect(() => readFile('does/not/exist')).to.throw(Error, /ENOENT/);
    });
    it('should read the example file', () => {
      const fileContents = readFile('./test/fixture/sampleFile.txt');
      expect(fileContents).to.not.be.null;
      expect(fileContents).to.be.equal(sampleFileContents);
    });
    it('should read the 2 example file', () => {
      const fileContents = readFile('./test/fixture/sampleFile2.txt');
      expect(fileContents).to.not.be.null;
      expect(fileContents).to.be.equal(sampleFile2Contents);
    });
  });
  describe('arrayifyFileContents scenarios.', () => {
    it('should be not null', () => {
      expect(arrayifyFileContents).to.not.be.null;
    });
    it('should throw error if path isn\'t a string', () => {
      expect(() => arrayifyFileContents({})).to.throw(TypeError);
    });
    it('should parse the example file and return an array', () => {
      const parsedData = arrayifyFileContents(sampleFileContents);
      expect(parsedData).to.be.instanceOf(Array).and.have.length(1);
      expect(parsedData).to.be.deep.equal([sampleFileContents]);
    });
    it('should read the example file', () => {
      const parsedData = arrayifyFileContents(sampleFile2Contents);
      expect(parsedData).to.be.instanceOf(Array).and.have.length(5);
      expect(parsedData).to.be.deep.equal([1, 2, 3, 4, 5]);
    });
  });
});
