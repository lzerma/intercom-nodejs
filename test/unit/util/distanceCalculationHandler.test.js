const expect = require('chai').expect;
const { calculateDistance } = require('../../../util/distanceCalculationHandler');

describe('distanceCalculation handler suite.', () => {
  describe('calculateDistance scenarios.', () => {
    it('should be not null', () => {
      expect(calculateDistance).to.not.be.null;
    });
    it('should throw error when null is given', () => {
      expect(calculateDistance).to.throw(TypeError);
    });
    it('should throw error when not enough params are given', () => {
      expect(() => calculateDistance(1212.121, 21341.12)).to.throw(TypeError);
    });
    it('should calculated the distance correctly for same origin and destination.', () => {
      const distance = calculateDistance(53.3393, -6.2576841, 53.3393, -6.2576841);
      expect(distance).to.be.not.null;
      expect(distance).to.be.equal(0); // the distance between the origin and the destiny is always 0
    });
    it('should calculated the distance correctly for origin and a diff destination.', () => {
      const distance = calculateDistance(52.986375, -6.043701, 53.3393, -6.2576841);
      expect(distance).to.be.not.null;
      // rounding it up.
      expect(Number(distance).toFixed(2)).to.be.equal('41.76');
    });
    it('should throw errors.', () => {
      expect(() => calculateDistance('52.986375', -6.043701, 53, {})).to.throw(TypeError);
    });
    it('should calculated the crazy distance correctly for origin and destination.', () => {
      const distance = calculateDistance(-12981291829, 102918290182, -1801289108, 6.2576841);
      expect(distance).to.not.be.null;
      expect(distance).to.be.equal(11832.067156831505);
    });
  });
});
