const expect = require('chai').expect;
const { flattenArray } = require('../../../util/arrayHandler');

describe('Array handler suite.', () => {
  describe('flattenArray scenarios.', () => {
    it('should be not null', () => {
      expect(flattenArray).to.not.be.null;
    });
    it('should return error if is not an array.', () => {
      expect(flattenArray).to.throw(TypeError);
    });
    it('should return empty array if is an empty array.', () => {
      expect(() => flattenArray(null)).to.throw(TypeError);
    });
    it('should return empty array if is an empty array.', () => {
      const flattenedArray = flattenArray([]);
      expect(flattenedArray).to.be.an.instanceOf(Array).and.deep.equal([]);
    });
    it('should be able to flatten an array `[[1, 2, [3]], 4]`', () => {
      const flattenedArray = flattenArray([[1, 2, [3]], 4]);
      expect(flattenedArray).to.be.an.instanceOf(Array).and.deep.equal([1, 2, 3, 4]);
    });
    it('should be able to flatten an array `[[1, 2, [3]], 4, [[[[5]]]]]`', () => {
      const flattenedArray = flattenArray([[1, 2, [3]], 4, [[[[5]]]]]);
      expect(flattenedArray).to.be.an.instanceOf(Array).and.deep.equal([1, 2, 3, 4, 5]);
    });
    it('should be able to flatten an array `[[1, 2, [3]], 4, [[[[5]], 6, [7], [[8, [[[9]]]]]],10]]`', () => {
      const flattenedArray = flattenArray([[1, 2, [3]], 4, [[[[5]], 6, [7], [[8, [[[9]]]]]], 10]]);
      expect(flattenedArray).to.be.an.instanceOf(Array).and.deep.equal([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    });
  });
});
