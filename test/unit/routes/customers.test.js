const expect = require('chai').expect;
const { get, loadCustomers, calcCustomersDistToDestiny } = require('../../../routes/customers');

describe('Unit - Customers route suite.', () => {
  describe('get() scenarios.', () => {
    it('should be not null', () => {
      expect(get).to.not.be.null;
    });
  });
  describe('loadCustomers() scenarios.', () => {
    it('should be not null', () => {
      expect(loadCustomers).to.not.be.null;
    });
    it('should load customers properly', () => {
      const customers = loadCustomers();
      const keys = ['latitude', 'user_id', 'name', 'longitude'];
      expect(customers).to.not.be.null;
      expect(customers).to.be.an.instanceOf(Array).and.to.have.length(32);
      expect(customers[0]).to.be.an('object').that.has.all.keys(keys);
    });
  });
  describe('calcCustomersDistToDestiny() scenarios.', () => {
    it('should be not null', () => {
      expect(calcCustomersDistToDestiny).to.not.be.null;
    });
    it('should throw an error if point is not defined.', () => {
      const point = null;
      const customers = loadCustomers();
      expect(() => calcCustomersDistToDestiny(customers, point)).to.throw(TypeError);
    });
    it('should throw an error if customers is not an array.', () => {
      const point = { latitude: 53.3393, longitude: -6.2576841 };
      const customers = { a: 1, b: 2 };
      expect(() => calcCustomersDistToDestiny(customers, point)).to.throw(TypeError);
    });
    it('should calculate customers distance properly', () => {
      const point = { latitude: 53.3393, longitude: -6.2576841 };
      const keys = ['latitude', 'user_id', 'name', 'longitude', 'distance'];
      let customers = loadCustomers();
      customers = calcCustomersDistToDestiny(customers, point);
      expect(customers).to.be.an.instanceOf(Array).and.to.has.length(32);
      expect(customers[0]).to.be.an('object').that.has.all.keys(keys);
    });
  });
});
