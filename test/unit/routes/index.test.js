const expect = require('chai').expect;
const { get } = require('../../../routes/index');

describe('Unit - Index route suite.', () => {
  describe('get() scenarios.', () => {
    it('should be not null', () => {
      expect(get).to.not.be.null;
    });
  });
});
