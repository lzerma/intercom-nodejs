const chai = require('chai');
const supertest = require('supertest');
const server = require('../../../app');

const expect = chai.expect;
const api = supertest(server);

describe('Integration - Index route suite.', () => {
  describe('get() scenarios.', () => {
    it('should get a valid response', (done) => {
      api.get('/')
      .expect('Content-Type', /json/)
      .expect((response) => {
        expect(response).to.have.property('statusCode', 200);
        expect(response.body).to.have.property('up', true);
      })
      .end(done);
    });
  });
});
