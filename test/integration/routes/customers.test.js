const chai = require('chai');
const supertest = require('supertest');
const server = require('../../../app');

const expect = chai.expect;
const api = supertest(server);

describe('Integration - Customers route suite.', () => {
  describe('get() scenarios.', () => {
    it('should get an error for not found', (done) => {
      api.get('/notfound')
      .expect('Content-Type', /json/)
      .expect((response) => {
        expect(response).to.have.property('statusCode', 404);
        expect(response.body).to.have.property('error', 'Not Found');
      })
      .end(done);
    });
    it('should get the costumer list', (done) => {
      api.get('/customers')
      .expect('Content-Type', /json/)
      .expect((response) => {
        expect(response).to.have.property('statusCode', 200);
        expect(response.body).to.be.not.null;
        expect(response.body).to.be.an.instanceOf(Array).and.have.length(16);
        expect(response.body[0]).to.be.an('object').that.has.all.keys('user_id', 'name');
        expect(response.body[0]).to.be.deep.equal({
          name: 'Ian Kehoe',
          user_id: 4
        });
        expect(response.body[response.body.length - 1]).to.be.deep.equal({
          name: 'Lisa Ahearn',
          user_id: 39
        });
      })
      .end(done);
    });
  });
});
